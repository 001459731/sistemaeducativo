﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SistemaEducativo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            // Establecer el título del formulario
            this.Text = "Registro Usuario";

            // Establecer el formato de los textos
            Font font = new Font("Algerian", 24, FontStyle.Bold); // Aumentar el tamaño de la fuente
            textBox1.Font = font;

            // Aumentar el tamaño del formulario para dar más espacio al título
            this.Size = new Size(200, 50); // Cambiar el tamaño del formulario

            // Eliminar el borde del cuadro de texto del título
            this.ControlBox = false;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            // No es necesario inicializar de nuevo los componentes ni cambiar el título aquí
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
